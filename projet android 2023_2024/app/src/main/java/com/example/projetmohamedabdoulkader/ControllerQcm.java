package com.example.projetmohamedabdoulkader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class ControllerQcm implements Serializable {
    private ArrayList<Questionnaire> questionnaires = new ArrayList<>();

    // Constructeur de la classe ControllerQcm
    public ControllerQcm(ArrayList<Questionnaire> questionnaires) {
        this.questionnaires = questionnaires;
    }

    // Getter pour récupérer la liste des questionnaires
    public ArrayList<Questionnaire> getQuestionnaires() {
        return questionnaires;
    }

    // Getter pour récupérer la liste des catégories de questionnaires
    public ArrayList<String> getCategorie() {
        ArrayList<String> categories = new ArrayList<>();
        for (Questionnaire questionnaire : questionnaires) {
            String category = questionnaire.getCategory();
            categories.add(category);
        }
        return categories;
    }

    // Getter pour récupérer la liste des catégories de questionnaires non terminés
    public ArrayList<String> getCategorieNotFinish() {
        ArrayList<String> categories = new ArrayList<>();
        for (Questionnaire questionnaire : questionnaires) {
            if (!questionnaire.getStatus()) {
                String category = questionnaire.getCategory();
                categories.add(category);
            }
        }
        return categories;
    }

    // Méthode pour récupérer un questionnaire en fonction de sa catégorie
    public Questionnaire getQuestionnaire(String selectedCategory) {
        for (Questionnaire questionnaire : questionnaires) {
            if (questionnaire.getCategory().equals(selectedCategory)) {
                return questionnaire;
            }
        }
        return null;
    }

    // Méthode pour vérifier si un questionnaire existe et retourner sa position
    public int existQuestionnaire(String category) {
        return questionnaires.indexOf(getQuestionnaire(category));
    }

    // Méthode pour récupérer la liste des questions en fonction de la catégorie
    public ArrayList<Question> getQuestions(String selectedCategory) {
        for (Questionnaire questionnaire : questionnaires) {
            if (questionnaire.getCategory().equals(selectedCategory)) {
                return (ArrayList<Question>) questionnaire.getQuestions();
            }
        }
        return null;
    }

    // Méthode pour supprimer une catégorie de questionnaires
    public void removeCategory(String categoryToRemove) {
        Iterator<Questionnaire> iterator = questionnaires.iterator();
        while (iterator.hasNext()) {
            Questionnaire questionnaire = iterator.next();
            if (questionnaire.getCategory().equals(categoryToRemove)) {
                iterator.remove();
                break;  // Sortir de la boucle car la catégorie est trouvée
            }
        }
    }

    // Méthode pour ajouter une nouvelle catégorie de questionnaires si elle n'existe pas déjà
    public void addCategory(String categoryToAdd) {
        boolean categoryExists = false;
        for (Questionnaire questionnaire : questionnaires) {
            if (questionnaire.getCategory().equals(categoryToAdd)) {
                categoryExists = true;
                break;
            }
        }

        if (!categoryExists) {
            Questionnaire newQuestionnaire = new Questionnaire(categoryToAdd, new ArrayList<>());
            questionnaires.add(newQuestionnaire);
        }
    }

    // Méthode pour ajouter un nouveau questionnaire à la liste
    public void addQuestionnaire(Questionnaire newQuestionnaire) {
        questionnaires.add(newQuestionnaire);
    }

    // Méthode pour charger le score d'un questionnaire et le marquer comme terminé
    public void loadScore(String cat, int score) {
        int position = existQuestionnaire(cat);
        if (position != -1) {
            questionnaires.get(position).setScoreQuestionnaire(score);
            questionnaires.get(position).setStatus(true);
        }
    }

    // Méthode pour réinitialiser tous les scores des questionnaires
    public void reinitScoreQcm() {
        questionnaires.forEach(questionnaire -> {
            questionnaire.setScoreQuestionnaire(0);
            questionnaire.setStatus(false);
        });
    }

    // Méthode pour calculer la moyenne des scores des questionnaires terminés
    public float calculMoyenne() {
        float totalPoint = 0;
        for (Questionnaire q : questionnaires) {
            if (q.getStatus()) {
                totalPoint += (float) q.getScore() * 20 / q.getSize();
            }
        }
        if (totalPoint == 0) return 0;
        return totalPoint / questionnaires.size();
    }
}
