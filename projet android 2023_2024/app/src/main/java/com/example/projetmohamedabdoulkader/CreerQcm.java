package com.example.projetmohamedabdoulkader;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CreerQcm extends AppCompatActivity {
    private EditText editText;
    private Button ajouter, retourner;
    String category;

    private ControllerQcm controllerQcm;

    // Méthode pour initialiser les éléments de l'interface utilisateur
    public void Initialisation(){
        editText = findViewById(R.id.categor2);
        ajouter = findViewById(R.id.btnValidate3);
        category = editText.getText().toString();
        retourner = findViewById(R.id.btnValidate4);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_qcm);
        Initialisation();

        Intent intent = getIntent();
        controllerQcm = (ControllerQcm) intent.getSerializableExtra("questionnaires");

        // Action du bouton "ajouter"
        ajouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(editText.getText())) {
                    // Affiche un message d'erreur si le champ est vide
                    Toast.makeText(CreerQcm.this, "Veuillez entrer une catégorie", Toast.LENGTH_SHORT).show();
                } else {
                    // Récupère la catégorie entrée par l'utilisateur
                    category = editText.getText().toString();

                    // Ajoute la nouvelle catégorie au ControllerQcm
                    controllerQcm.addCategory(category);

                    // Log pour vérifier les catégories après l'ajout
                    Log.d("questionnaires : ", String.valueOf(controllerQcm.getCategorie()));

                    // Passe à l'activité de création de questions avec la nouvelle catégorie
                    Intent intent = new Intent(CreerQcm.this, CreerQuestionsActivity.class);
                    intent.putExtra("category_cree", category);
                    intent.putExtra("questionnaires", controllerQcm);
                    startActivityForResult(intent, 20);
                }
            }
        });

        // Action du bouton "retourner"
        retourner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Retourne à l'activité principale avec le ControllerQcm mis à jour
                Intent intent = new Intent(CreerQcm.this, MainActivity.class);
                intent.putExtra("newQuestionnaire", controllerQcm);
                intent.putExtra("newCategory", category);
                startActivityForResult(intent, 21);
            }
        });
    }
}
