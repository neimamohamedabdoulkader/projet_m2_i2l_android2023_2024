package com.example.projetmohamedabdoulkader;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreerQuestionsActivity extends AppCompatActivity {
    private EditText categoryEditText, questionEditText, option1EditText, option2EditText, option3EditText;
    private Button validateButton,exitButton;
    private String category_Intent;
    private ActivityResultLauncher<Intent> activityResultLauncher;
    private ControllerQcm controllerQcm;
    private Spinner correctIndexSpinner;
    private TextView categoryTextView;
    // creer un constant pour passer au setresult
    public static final int CREER_QUESTIONS = 5;
    private String selectedCategory;
    void initialisation(){
        questionEditText = findViewById(R.id.quest);
        option1EditText = findViewById(R.id.opt1);
        option2EditText = findViewById(R.id.op2);
        option3EditText = findViewById(R.id.op3);
        validateButton = findViewById(R.id.btnValidate2);
        exitButton = findViewById(R.id.btnStopQuiz2);
        correctIndexSpinner = findViewById(R.id.spinner);
         categoryTextView = findViewById(R.id.textcategorycreer);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_questions);
        initialisation();

        // Extract ControllerQcm and category from Intent
        Intent intent = getIntent();
        controllerQcm = intent.getSerializableExtra("questionnaires", ControllerQcm.class);

        Log.d("questionnaires : ",String.valueOf(controllerQcm.getCategorie()));
        selectedCategory = intent.getStringExtra("category_cree");
        controllerQcm.addCategory(selectedCategory);
        Log.d("questionnaires : ",String.valueOf(controllerQcm.getCategorie()));





        categoryTextView.setText(selectedCategory);
        // Set click listener for the exit button



        Button validateButton = findViewById(R.id.btnValidate2);
        validateButton.setOnClickListener(view -> {
            // Get user-input values
            String questionText = questionEditText.getText().toString();
            String option1 = option1EditText.getText().toString();
            String option2 = option2EditText.getText().toString();
            String option3 = option3EditText.getText().toString();
            int correctIndex = correctIndexSpinner.getSelectedItemPosition();
            // lOG
            Log.d("Question", questionText);
            Log.d("Option 1", option1);
            Log.d("Option 2", option2);
            Log.d("Option 3", option3);
            Log.d("Correct Index", String.valueOf(correctIndex));

            // Validate spinner selection
            if (correctIndex == Spinner.INVALID_POSITION) {
                Toast.makeText(this, "Please choose a correct answer in the spinner", Toast.LENGTH_SHORT).show();
                return; // Exit the method if the spinner selection is not valid
            }
            // Create a new Question object
            Question newQuestion = new Question(questionText, new String[]{option1, option2, option3}, correctIndex);

            // Find the corresponding Questionnaire
            Questionnaire questionnaire = controllerQcm.getQuestionnaire(selectedCategory);

            // Add the new question to the Questionnaire
            if (questionnaire != null) {
                questionnaire.addQuestion(newQuestion);
                Log.d("Questionnaire", questionnaire.toString());
            }
            if (controllerQcm != null) {
                // You might not need to add the questionnaire again,
                // as it's already updated in the previous step
                Log.d("ControllerQcm", controllerQcm.toString());
            }

            // Save the updated questionnaire
            saveQuestionnaireToFile(questionnaire);


        });
        exitButton.setOnClickListener(view -> {
            Intent intent1 = new Intent(CreerQuestionsActivity.this, StartQuestionnaire.class);
            intent1.putExtra("questionnaires", controllerQcm);
            Log.d("questionnaires : ",String.valueOf(controllerQcm.getCategorie()));
            startActivityForResult(intent1, 40);
        });
    }

    private Questionnaire createQuestionnaire() {


        String question = questionEditText.getText().toString().trim();
        String option1 = option1EditText.getText().toString().trim();
        String option2 = option2EditText.getText().toString().trim();
        String option3 = option3EditText.getText().toString().trim();

        // Create options array
        String[] options = {option1, option2, option3};

        // Identify the correct answer index on the spinner
        int correctAnswerIndex = correctIndexSpinner.getSelectedItemPosition();


        // Create Question object
        Question newQuestion = new Question(question, options, correctAnswerIndex);
        // log question
        Log.d("Question", newQuestion.getQuestion());
        Log.d("Option 1", newQuestion.getOptions()[0]);
        Log.d("Option 2", newQuestion.getOptions()[1]);
        Log.d("Option 3", newQuestion.getOptions()[2]);
        Log.d("Correct Index", String.valueOf(newQuestion.getCorrectAnswerIndex()));



        // Create Questionnaire object and add the question
        Questionnaire newQuestionnaire = new Questionnaire(category_Intent, new ArrayList<>());
        // log questionnaire
        Log.d("Questionnaire", newQuestionnaire.getCategory());
        Log.d("Questionnaire", String.valueOf(newQuestionnaire.getQuestions()));

        newQuestionnaire.addQuestion(newQuestion);
        // log questionnaire
        Log.d("Questionnaire", String.valueOf(newQuestionnaire.getQuestions()));

        return newQuestionnaire;

    }
    private void  SaveQuestionnaire() {
        Questionnaire newQuestionnaire = createQuestionnaire();
        saveQuestionnaireToFile(newQuestionnaire);
        Intent intent = new Intent(CreerQuestionsActivity.this, StartQuestionnaire.class);
        intent.putExtra("questionnaires", newQuestionnaire);
        controllerQcm.addQuestionnaire(newQuestionnaire);

        setResult(CREER_QUESTIONS, intent);
        finish();
    }


    /**
     * Sauvegarde le questionnaire dans un fichier texte.
     *
     * @param questionnaire Le questionnaire à sauvegarder.
     */
    private void saveQuestionnaireToFile(Questionnaire questionnaire) {
        // Nom du dossier et du fichier
        String folderName = "QCM";
        String fileName = selectedCategory.replaceAll("\\s+", "_").toLowerCase() + ".txt";

        // Répertoire où le fichier sera sauvegardé
        File folder = new File(getFilesDir(), folderName);

        // Vérifie si le dossier existe, sinon le crée
        if (!folder.exists()) {
            folder.mkdir();
        }

        // Crée le fichier dans le répertoire
        File file = new File(folder, fileName);

        // StringBuilder pour construire le contenu du fichier
        StringBuilder stringBuilder = new StringBuilder();

        // Récupère les données existantes du fichier, le cas échéant
        try {
            FileInputStream fis = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));
            String line;
            boolean categoryWritten = false;

            // Parcourt les lignes du fichier
            while ((line = bufferedReader.readLine()) != null) {
                // Vérifie si la catégorie a déjà été écrite
                if (!categoryWritten && line.trim().equals(questionnaire.getCategory())) {
                    categoryWritten = true;
                }

                stringBuilder.append(line).append("\n");
            }
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Vérifie si la catégorie a déjà été écrite
        if (!stringBuilder.toString().contains(questionnaire.getCategory())) {
            stringBuilder.append(questionnaire.getCategory()).append("\n\n");
        }

        // Récupère la liste des questions du questionnaire
        List<Question> questions = questionnaire.getQuestions();

        // Parcourt les questions et construit les blocs de questions formatés
        for (Question question : questions) {
            // Construit le bloc de question formaté
            String formattedQuestionBlock = buildFormattedQuestionBlock(question);

            // Vérifie si le bloc de question formaté existe déjà dans le fichier
            if (!stringBuilder.toString().contains(formattedQuestionBlock)) {
                stringBuilder.append(formattedQuestionBlock);
            }
        }

        try {
            // Crée un nouveau FileOutputStream en mode ajout
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(stringBuilder.toString().getBytes());
            fos.close();

            // Réinitialise les champs après la sauvegarde
            questionEditText.setText("");
            option1EditText.setText("");
            option2EditText.setText("");
            option3EditText.setText("");
            Toast.makeText(this, "Questionnaire for " + selectedCategory + " saved successfully", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error saving questionnaire for " + selectedCategory, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Construit un bloc de question formaté à partir d'un objet Question.
     *
     * @param question La question pour laquelle construire le bloc formaté.
     * @return Le bloc de question formaté.
     */
    private String buildFormattedQuestionBlock(Question question) {
        // StringBuilder pour construire le bloc formaté
        StringBuilder formattedQuestionBlock = new StringBuilder();
        formattedQuestionBlock.append(question.getQuestion()).append("\n\n");

        // Options de la question
        String[] options = question.getOptions();

        // Parcourt les options et construit le bloc de question formaté
        for (int i = 0; i < options.length; i++) {
            // Ajoute une tabulation et "x" à l'option correcte
            formattedQuestionBlock.append("  ").append(options[i]).append(i == question.getCorrectAnswerIndex() ? " x" : "").append("\n");
        }

        formattedQuestionBlock.append("\n\n");

        return formattedQuestionBlock.toString();
    }



}