package com.example.projetmohamedabdoulkader;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Button score, creer_questionnaire, quitter,commence_quiz;
    private List<Question> questionsList = new ArrayList<>();
    private ControllerQcm controllerQcm;

    private void initializeButtons() {
        score = findViewById(R.id.score);
        creer_questionnaire = findViewById(R.id.creer_question);
        quitter = findViewById(R.id.exit);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeButtons();
        Intent data = getIntent();
        controllerQcm  = data.getSerializableExtra("qcm", ControllerQcm.class);
        if(controllerQcm==null){
            ArrayList<Questionnaire> loadedQuestionnaires = loadQcmDataFromBoth(getApplicationContext());
            controllerQcm = new ControllerQcm(loadedQuestionnaires);
            readScore();
        }
        setButtonClickListeners();
        Toolbar toolbar = findViewById(R.id.toolbar3);
        setSupportActionBar(toolbar);


    }
    // Load data from both res/raw and files/QCM folders
    public static ArrayList<Questionnaire> loadQcmDataFromBoth(Context context) {
        ArrayList<Questionnaire> questionnaires = new ArrayList<>();

        // Load data from res/raw folder
        try {
            Resources res = context.getResources();
            Field[] fields = R.raw.class.getFields();
            for (Field field : fields) {
                int resId = field.getInt(null);
                InputStream inputStream = res.openRawResource(resId);
                Questionnaire questionnaire = parseQcm(inputStream);
                questionnaires.add(questionnaire);
                inputStream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Load data from files/QCM folder
        try {
            File qcmFolder = new File(context.getFilesDir(), "QCM");
            File[] files = qcmFolder.listFiles();

            if (files != null) {
                for (File file : files) {
                    InputStream inputStream = new FileInputStream(file);
                    Questionnaire questionnaire = parseQcm(inputStream);
                    questionnaires.add(questionnaire);
                    inputStream.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return questionnaires;
    }






    public static Questionnaire parseQcm(InputStream inputStream) {
        List<Question> questions = new ArrayList<>();
        String category = null;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            String currentQuestion = null;
            List<String> options = new ArrayList<>();
            int correctAnswerIndex = -1;

            while ((line = reader.readLine()) != null) {
                line = line.trim();

                if (line.isEmpty()) {
                    continue; // Ignorer les lignes vides
                }

                if (category == null) {
                    category = line; // Première ligne comme catégorie
                    Log.d("Question Category", category);
                    continue;
                }

                if (currentQuestion == null) {
                    currentQuestion = line; // Récupérer la question
                } else if (options.size() < 3) {
                    options.add(line); // Ajouter les options
                    if (line.endsWith("x")) {
                        correctAnswerIndex = options.size() - 1;
                        // Supprimer le "x" de la réponse correcte
                        options.set(correctAnswerIndex, options.get(correctAnswerIndex).replace("x", ""));

                    }
                }

                // Si les options sont complètes, créer une question
                if (options.size() == 3) {
                    String[] optionsArray = options.toArray(new String[0]);
                    Question question = new Question(currentQuestion, optionsArray, correctAnswerIndex);
                    questions.add(question);
                    question.getQuestion();
                    question.getOptions();
                    question.getCorrectAnswerIndex();


                    // Réinitialisation pour la prochaine question
                    currentQuestion = null;
                    options.clear();
                    correctAnswerIndex = -1;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Questionnaire q = new Questionnaire(category, questions);
        q.getCategory();
        q.getQuestions();

        return q;
    }
    private void requestAdminPassword() {
        // Create an AlertDialog to prompt for the password
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter Administrator Password");

        // Set up the input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String password = input.getText().toString().trim();
                // Check if the entered password matches the predefined password "MDP"
                if (password.equals("MDP")) {
                    // Password is correct, proceed with creating questionnaires
                    startCreerQuestionnaireActivity();

                } else {
                    // Incorrect password, show a toast or message indicating the error
                    Toast.makeText(MainActivity.this, "Incorrect password", Toast.LENGTH_SHORT).show();
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Show the dialog
        builder.show();
    }
    private void startCreerQuestionnaireActivity() {
        // Use the loadQcmDataFromBoth function to load data from both res/raw and files/QCM
        ArrayList<Questionnaire> loadedQuestionnaires = loadQcmDataFromBoth(getApplicationContext());

        Intent intent = new Intent(MainActivity.this, CreerQcm.class);
        intent.putExtra("questionnaires", new ControllerQcm(loadedQuestionnaires));
        startActivityForResult(intent, 20);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.view_score) {
            displayAllScores();
            return true;
        }

        if (itemId == R.id.reinistialiser) {
            startReinistialiserScoreActivity();
            return true;
        }

        if (itemId == R.id.quitter) {
            // enregistrer les score dans le fichier
            saveScore();
            finish();
            return true;
        }
        if (itemId == R.id.commencer_quiz) {
            startUser_Interface_Activity();
            finish();
            return true;
        }

        if (itemId == R.id.creer_questionnaire) {
            requestAdminPassword();
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

    private void setButtonClickListeners() {

        quitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // enregistrer les score dans le fichier
                saveScore();
                finish();
            }
        });

        // commencer le quiz
        findViewById(R.id.commence_quiz).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startUser_Interface_Activity();
                finish();

            }
        });
        creer_questionnaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestAdminPassword();
            }
        });
        // reinstialiser les scores
        findViewById(R.id.res_core).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startReinistialiserScoreActivity();
            }
        });
        // visualiser les scores
        findViewById(R.id.score).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayAllScores();
            }
        });
    }


    private void displayAllScores() {

        // Create an intent to start the ScoreActivity
        Intent intent = new Intent(MainActivity.this, ScoreActivity.class);
        intent.putExtra("qcm", controllerQcm);
        startActivity(intent);
        finish();
    }


    private void startUser_Interface_Activity() {
        // Use the loadQcmDataFromBoth function to load data from both res/raw and files/QCM

        Intent intent = new Intent(MainActivity.this, StartQuestionnaire.class);
        intent.putExtra("questionnaires", controllerQcm);
        startActivity(intent);
    }

    public void readScore() {
        String filename ="score.txt"; // Utilise le nom de la catégorie pour créer un fichier distinct
        try {
            FileInputStream fis = openFileInput(filename); // Utilise MODE_APPEND pour ajouter des données au fichier existant
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while((line=br.readLine())!=null){
                String cat = line.trim().split(":")[0];
                int score = Integer.parseInt(line.trim().split(":")[1]);
                controllerQcm.loadScore(cat, score);
            }

            Log.d("SaveScore", "Score saved successfully for category: ");
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("SaveScore", "Error saving score for category:");
        }
    }

    private void saveScore() {
        String filename ="score.txt"; // Utilise le nom de la catégorie pour créer un fichier distinct


        try {
            FileOutputStream fos = openFileOutput(filename, Context.MODE_PRIVATE); // Utilise MODE_APPEND pour ajouter des données au fichier existant
            OutputStreamWriter osw = new OutputStreamWriter(fos);
            for(Questionnaire questionnaire: controllerQcm.getQuestionnaires()){
                if(questionnaire.getStatus()){
                    Log.d("status questionnaire", String.valueOf(questionnaire.getStatus()));
                    try {
                        osw.write(questionnaire.getCategory()+":"+ questionnaire.getScore()+"\n");
                    } catch (IOException e) {
                        Log.d("error", "error file whirting");
                    }
                }
            }
            osw.close();
            fos.close();
            Log.d("SaveScore", "Score saved successfully for category: ");
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("SaveScore", "Error saving score for category:");
        }
    }

    private void startReinistialiserScoreActivity() {
        // Create an AlertDialog to confirm score reset
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Réinitialiser les scores");
        builder.setMessage("Êtes-vous sûr de vouloir réinitialiser les scores?");

        // Set up the buttons
        builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Reset the scores
                controllerQcm.reinitScoreQcm();
                Toast.makeText(MainActivity.this, "Scores réinitialisés", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.cancel();

            }
        });

        // Show the dialog
        builder.show();
    }


}