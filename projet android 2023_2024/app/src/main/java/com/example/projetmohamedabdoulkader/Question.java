package com.example.projetmohamedabdoulkader;

import java.io.Serializable;

public class Question implements Serializable {

    private String question;
    public String[] options;
    private int correctAnswerIndex;

    // Constructeur
    public Question(String question, String[] options, int correctAnswerIndex) {

        this.question = question;
        this.options = options;
        this.correctAnswerIndex = correctAnswerIndex;
    }

    // Getters


    public String getQuestion() {
        return question;
    }

    public String[] getOptions() {
        return options;
    }

    public int getCorrectAnswerIndex() {
        return correctAnswerIndex;
    }

    public String getCorrectAnswer(){
        return options[correctAnswerIndex];
    }
}