package com.example.projetmohamedabdoulkader;


import java.io.Serializable;
import java.util.List;

public class Questionnaire implements Serializable {
    String category;
    private List<Question> questions;
    private int scoreQuestionnaire;
    private boolean status;

    /**
     * Constructeur de la classe Questionnaire.
     *
     * @param category   La catégorie du questionnaire.
     * @param questions  Liste des questions du questionnaire.
     */
    public Questionnaire(String category, List<Question> questions) {
        this.category = category;
        this.questions = questions;
        status = false;  // Le statut est initialement défini sur false.
        scoreQuestionnaire = 0;  // Le score du questionnaire est initialement défini sur 0.
    }

    /**
     * Obtient la catégorie du questionnaire.
     *
     * @return La catégorie du questionnaire.
     */
    public String getCategory() {
        System.out.println("Category: " + category);
        return category;
    }

    /**
     * Ajoute une question à la liste des questions du questionnaire.
     *
     * @param question La question à ajouter.
     */
    public void addQuestion(Question question) {
        questions.add(question);
    }

    /**
     * Obtient la liste des questions du questionnaire.
     *
     * @return La liste des questions du questionnaire.
     */
    public List<Question> getQuestions() {
        // Parcourt la liste et affiche les détails de chaque question
        for (Question question : questions) {
            System.out.println("Question Text: " + question.getQuestion());

            String[] options = question.getOptions();
            for (int i = 0; i < options.length; i++) {
                System.out.println("Option " + (i + 1) + ": " + options[i]);
            }

            System.out.println("Correct Answer Index: " + question.getCorrectAnswerIndex());
            System.out.println("------------");
        }

        return questions;
    }

    /**
     * Définit le score du questionnaire.
     *
     * @param score Le score à attribuer au questionnaire.
     */
    public void setScoreQuestionnaire(int score) {
        scoreQuestionnaire = score;
    }

    /**
     * Définit le statut du questionnaire (complété ou non).
     *
     * @param status Le statut à définir.
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * Obtient le score du questionnaire.
     *
     * @return Le score du questionnaire.
     */
    public int getScore() {
        return scoreQuestionnaire;
    }

    /**
     * Obtient la taille (nombre de questions) du questionnaire.
     *
     * @return La taille du questionnaire.
     */
    public int getSize() {
        return questions.size();
    }

    /**
     * Obtient le statut du questionnaire (complété ou non).
     *
     * @return Le statut du questionnaire.
     */
    public boolean getStatus() {
        return status;
    }

}