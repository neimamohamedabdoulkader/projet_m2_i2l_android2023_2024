package com.example.projetmohamedabdoulkader;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;



    /**
     * Activité pour afficher les scores des questionnaires et la note moyenne.
     */
    public class ScoreActivity extends AppCompatActivity {
        public static final int SCORE_OK = 3;
        private TextView textCategory, textMoyenne, text;
        private Button okbutton;
        private ControllerQcm controllerQcm;
        private LinearLayout linearLayout;
        String garder;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_score);

            okbutton = findViewById(R.id.okbtn);

            // Récupère les données de l'intent
            Intent data = getIntent();
            // Récupère le contrôleur de questionnaires
            controllerQcm = data.getSerializableExtra("qcm", ControllerQcm.class);

            linearLayout = findViewById(R.id.linearLayoutScore);

            // Récupère la catégorie
            String category = data.getStringExtra("category");
            garder = category;

            // Ajoute un écouteur de clic pour le bouton OK
            okbutton.setOnClickListener(v -> {
                // Crée une nouvelle intention pour revenir à l'activité principale
                Intent intent = new Intent(ScoreActivity.this, MainActivity.class);
                intent.putExtra("qcm", controllerQcm);
                startActivity(intent);  // Utilise startActivity au lieu de setResult et finish
                finish();
            });

            // Affiche les scores
            displayScores();
        }

        /**
         * Affiche les scores des questionnaires et la note moyenne.
         */
        private void displayScores() {
            // Parcourt les questionnaires dans le contrôleur
            controllerQcm.getQuestionnaires().forEach(questionnaire -> {
                // Crée un nouveau TextView pour afficher le score du questionnaire
                TextView textView = new TextView(this);
                String showScore = String.format("Note questionnaire %s \t %s/%s", questionnaire.getCategory(), questionnaire.getScore(), questionnaire.getSize());
                textView.setText(showScore);
                linearLayout.addView(textView);  // Ajoute le TextView au LinearLayout
            });

            // Affiche la note moyenne
            TextView textView = new TextView(this);
            textView.setText("Note moyenne \t " + controllerQcm.calculMoyenne() + "/20");
            linearLayout.addView(textView);  // Ajoute le TextView au LinearLayout
        }
    }



