package com.example.projetmohamedabdoulkader;

import static com.example.projetmohamedabdoulkader.CreerQuestionsActivity.CREER_QUESTIONS;
import static com.example.projetmohamedabdoulkader.ScoreActivity.SCORE_OK;
import static com.example.projetmohamedabdoulkader.StartQuiz.STOP_QUIZ;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class StartQuestionnaire extends AppCompatActivity {
    private Button btnStartQuiz,retourner;
    private ListView listView;
    private String selectedCategory;
    private Toast toast;
    private ArrayAdapter<String> arrayAdapter;
    private ControllerQcm controllerQcm;
    private ActivityResultLauncher<Intent> startQuestionnaireLauncher;
    private String finishCategory;
    private List<String> finishedCategories = new ArrayList<>();
    private boolean allQuestionsFinished;
    void initializeButtons() {
        btnStartQuiz = findViewById(R.id.btnStartQuiz);
        listView = findViewById(R.id.list);
        retourner= findViewById(R.id.btnStartQuiz2);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_questionnaire);
        initializeButtons();
        startQuestionnaireLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK) {
                       Questionnaire questionnairePassed = result.getData().getSerializableExtra("questionnairePassed", Questionnaire.class);
                       int position = controllerQcm.existQuestionnaire(questionnairePassed.getCategory());
                       if(position!=-1){
                           controllerQcm.getQuestionnaires().set(position, questionnairePassed);
                           arrayAdapter.remove(questionnairePassed.getCategory());
                           arrayAdapter.notifyDataSetChanged();
                       }
                       Intent intent = new Intent(StartQuestionnaire.this, ScoreActivity.class);
                       intent.putExtra("qcm", controllerQcm);
                       startQuestionnaireLauncher.launch(intent);
                    }

                });
        displayCategories();

        btnStartQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startQuiz();
            }
        });
        retourner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(StartQuestionnaire.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
    String garder;
    private void displayCategories() {
        Intent data = getIntent();

        controllerQcm = data.getSerializableExtra("questionnaires",ControllerQcm.class);    // Récupérer la liste des catégories de mainActivity

        ArrayList<String> categories = controllerQcm.getCategorieNotFinish();

        // Créer un ArrayAdapter qui prend la liste des catégories et l'affiche dans le ListView
        arrayAdapter = new ArrayAdapter<>(StartQuestionnaire.this, android.R.layout.simple_list_item_single_choice, categories);
        listView.setAdapter(arrayAdapter);

        // Ajouter un listener sur le ListView pour récupérer la catégorie sélectionnée
        listView.setOnItemClickListener((parent, view, position, id) -> {
            selectedCategory = categories.get(position);
            toast = Toast.makeText(getApplicationContext(), "Catégorie sélectionnée: " + selectedCategory, Toast.LENGTH_SHORT);
            toast.show();

        });
    }
    private void startQuiz() {
        if (selectedCategory != null) {
            Intent intent = new Intent(StartQuestionnaire.this, StartQuiz.class);
            // Set the selected category and pass it to StartQuiz activity
            intent.putExtra("selectedCategory", controllerQcm.getQuestionnaire(selectedCategory));

            // Pass the ControllerQcm and other necessary data
            //intent.putExtra("questionnaires", controllerQcm);
            //intent.putExtra("categories", controllerQcm.getCategorie());

            startQuestionnaireLauncher.launch(intent);

            toast = Toast.makeText(getApplicationContext(), "Catégorie sélectionnée: " + selectedCategory, Toast.LENGTH_SHORT);
        } else {
            toast = Toast.makeText(getApplicationContext(), "Veuillez sélectionner une catégorie", Toast.LENGTH_SHORT);
        }
    }



}