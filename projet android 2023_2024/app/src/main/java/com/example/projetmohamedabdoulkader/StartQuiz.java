package com.example.projetmohamedabdoulkader;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StartQuiz extends AppCompatActivity {
    public static final int NOTIFY_MAIN =11 ;
    public static final int SCORE =19 ;
    private TextView textCategory, textQuestionNumber, textQuestion;
    private Button btnStopQuiz, btnValidate;
    private RadioGroup radioGroup;    private boolean allQuestionsFinished = false;
    private int totalScore = 0;
    private RadioButton radioButtonChoice1, radioButtonChoice2, radioButtonChoice3;
    // create a constant stopbutton to stop the quiz
    public static final int STOP_QUIZ = 10;
    private Questionnaire selectQuestionnaire;

    private ArrayList<Question> shuffledQuestions;
    private int currentQuestionIndex = 0;
private String selectedCategory;

    public void intialise() {
        // Récupération des ID des éléments de la mise en page
        textCategory = findViewById(R.id.textCategory);
        textQuestionNumber = findViewById(R.id.textQuestionNumber);
        textQuestion = findViewById(R.id.textQuestion);
        btnStopQuiz = findViewById(R.id.btnStopQuiz);
        btnValidate = findViewById(R.id.btnValidate);
        radioGroup = findViewById(R.id.g);
        radioButtonChoice1 = findViewById(R.id.radioButtonChoice3);
        radioButtonChoice2 = findViewById(R.id.radioButtonChoice1);
        radioButtonChoice3 = findViewById(R.id.radioButtonChoice2);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_quiz);
        intialise();
        getData();
        btnStopQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle stopping the quiz
                stopQuizAndClearScore();
            }
        });
        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle validating the answer
                validateAnswer();
              //  finishQuiz();
            }
        });
        displayQuestion();

    }
    // fonctionner  qui recupere les donnes envoye par startquestionnaire
    private void getData() {
        Intent data = getIntent();

        if (data != null) {
            selectQuestionnaire =  data.getSerializableExtra("selectedCategory" , Questionnaire.class);
            //controllerQcm = data.getSerializableExtra("questionnaires", ControllerQcm.class);
                if (selectQuestionnaire != null) {
                    textCategory.setText("Catégorie : " + selectQuestionnaire.getCategory());
                    if (selectQuestionnaire.getQuestions() != null) {

                        // Shuffle the questions
                        shuffledQuestions = new ArrayList<>(selectQuestionnaire.getQuestions());
                        Collections.shuffle(shuffledQuestions);

                        // Now you can proceed to display the first question
                        displayQuestion();
                    } else {
                        Log.e(TAG, "Questions object or question list is null");
                    }
                } else {
                    Log.e(TAG, "Selected category is null");
                }
            } else {
                Log.e(TAG, "ControllerQcm object is null");
            }
    }

    private void displayQuestion() {
        // Display the current question
        if (currentQuestionIndex < shuffledQuestions.size()) {
            Question currentQuestion = shuffledQuestions.get(currentQuestionIndex);


            textQuestionNumber.setText("Question " + (currentQuestionIndex + 1) + " sur " + shuffledQuestions.size());
            textQuestion.setText(currentQuestion.getQuestion());

            // Shuffle the options for display
            ArrayList<String> shuffledOptions = new ArrayList<>(Arrays.asList(currentQuestion.getOptions()));
            Collections.shuffle(shuffledOptions);

            radioButtonChoice1.setText(shuffledOptions.get(0));
            radioButtonChoice2.setText(shuffledOptions.get(1));
            radioButtonChoice3.setText(shuffledOptions.get(2));
            // Clear the checked state of the RadioGroup
            radioGroup.clearCheck();
        } else {
            sendScore(); // No more questions, finish the quiz
            Toast.makeText(getApplicationContext(), "Score : " + totalScore, Toast.LENGTH_SHORT).show();
        }
    }
    private void validateAnswer() {
        int selectedRadioButtonId = radioGroup.getCheckedRadioButtonId();
        String selectedAnswerText = "";

        if (selectedRadioButtonId != -1) {
            RadioButton selectedRadioButton = findViewById(selectedRadioButtonId);
            selectedAnswerText = selectedRadioButton.getText().toString();
            Log.d("re", "Selected answer: " + selectedAnswerText);
        } else {
            Toast.makeText(getApplicationContext(), "Veuillez sélectionner une réponse", Toast.LENGTH_SHORT).show();
            return;
        }

        if (currentQuestionIndex < shuffledQuestions.size()) {
            Question currentQuestion = shuffledQuestions.get(currentQuestionIndex);
            String correctAnswerText = currentQuestion.getCorrectAnswer();
            Log.d("re", "Correct answer: " + correctAnswerText);

            // Check if the selected answer is correct and update totalScore
            if (selectedAnswerText.equalsIgnoreCase(correctAnswerText)) {
                totalScore++; // Increment total score upon correct answer
            }

       }

        currentQuestionIndex++;

        // Display the next question or finish the quiz
        if (currentQuestionIndex < shuffledQuestions.size()) {
            displayQuestion();
        } else {
            selectQuestionnaire.setScoreQuestionnaire(totalScore);
            selectQuestionnaire.setStatus(true);
            Toast.makeText(getApplicationContext(), "Score : " + totalScore, Toast.LENGTH_SHORT).show();
            // save the score
            //clearScoreFile(selectedCategory);
            //saveScore(totalScore, selectedCategory, questions.getQuestions().size());
            sendScore();
        }
    }



    private void sendScore() {
       Intent intent = new Intent(StartQuiz.this, StartQuestionnaire.class);
        intent.putExtra("questionnairePassed", selectQuestionnaire);

        setResult(RESULT_OK, intent);
        // Create an intent with the finish category and score
        finish();
    }


    // Method to save the score



    private void stopQuizAndClearScore() {
        // Finish the quiz and send the result to StartQuestionnaire
        Intent resultIntent = new Intent(StartQuiz.this,StartQuestionnaire.class);
        setResult(RESULT_CANCELED, resultIntent);
        finish();
    }
}